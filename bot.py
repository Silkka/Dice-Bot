import discord
import asyncio
import re
import random
from NumericStringParser import NumericStringParser

token = ''
with open("token") as fh:
	token = fh.read().strip('\n')

#Commands
C_ROLL = '!r '
C_VERBOSE = 'verbose'

client = discord.Client()

#if number_of_dice_kept>0: keep number_of_dice_kept highest dice
#if number_of_dice_kept<0: keep number_of_dice_kept lowest dice
#if number_of_dice_kept=0: keep all of the dice
def get_rolls(number_of_dice, size_of_die, number_of_dice_kept):
    rolls = []
    for j in range(number_of_dice):
        rolls.append(random.randrange(1,size_of_die+1))
    rolls.sort(reverse=True)
    if number_of_dice_kept > 0:
        rolls = rolls[:number_of_dice_kept]
    elif number_of_dice_kept < 0:
        rolls = rolls[number_of_dice+number_of_dice_kept:]
    
    return rolls

#parse dice parameters from [0-9]*d[0-9]+(k-?[0-9]*)?
def parse_dice(dice_re):
    first_split = dice_re.group().split('d',maxsplit=1)

    if first_split[0] == '':
        number_of_dice = 1
    else:
        number_of_dice = first_split[0]
    
    second_split = first_split[1].split('k',maxsplit=1)
    
    size_of_die = second_split[0]
    number_of_dice_kept = number_of_dice
    if len(second_split) > 1:
        number_of_dice_kept = second_split[1]

    return [int(number_of_dice),int(size_of_die),int(number_of_dice_kept)]

def rolls_to_string(rolls):
    s = "("
    for j in rolls:
        s = s + str(j) + " + "
    s = s[:-3] + ")"
    return s
#Prepares an expression with dice markup in it
# "4d4k3 + 6 + 2d12" -> "(4+2+2) + 6 +  (12 + 7)"
def prepare_dice_expression(s,verbose=True):
    result = s
    a = re.finditer(r"[0-9]*d[0-9]+(k-?[0-9]*)?",result)
    for i in a:

        [number_of_dice,size_of_die,number_of_dice_kept] = parse_dice(i)

        #print([number_of_dice,size_of_die,number_of_dice_kept])
        
        rolls = get_rolls(number_of_dice, size_of_die, number_of_dice_kept)
        rolls_string = ""
        if verbose == True:
            rolls_string = rolls_to_string(rolls)
        else:
            rolls_string = str(sum(rolls))
        
        result = result.replace(i.group(),rolls_string,1)
    return result

def prepare_repeats(s,nsp,verbose=True):
    a = re.finditer(r"repeat\([\w \+\-\*\/\^]+,[\d ]+\)",s)
    for e in a:
        result="["
        expr = e.group()
        expr = expr[len('repeat('):-1]
        [dice_expression, repeats] = expr.split(',')
        repeats = int(repeats)
        for i in range(repeats):
            temp = prepare_dice_expression(dice_expression, verbose=verbose)
            if verbose == False:
                temp = str(int(nsp.eval(temp)))
            result= result + temp + ","
        result = result[:-1] + "]"
        s = s.replace(e.group(), result, 1)
    return s

def prepare_scalars(s,nsp):
    result = s
    a = re.finditer(r"\]?[\w \+\-\*\/\^]+[\+\-\*\/\^]? *\[",result)
    for i in a:
        expr = i.group()
        starts_with = ''
        if expr.startswith(']'):
            expr = expr[1:]
            starts_with='] +'
        tail = re.search(r"[\+\-\*\/\^] *\[",expr)
        if tail:
            operation = tail.group()[0]
            expr = expr[:-len(tail.group())]
            value = str(int(nsp.eval(expr)))
            result =  result.replace(i.group(),starts_with + " " + value + " " + operation + " [" , 1)
    return result

def insert_scalars_to_repeats(s,nsp):
    a = re.finditer(r"-?\d+ *[\+\-\*\/\^] *\[[\d\,]+\]",s)
    for i in a:
        expr = i.group()
        scalar = re.search(r"^-?[\d]+",expr).group()
        expr = expr[len(scalar):]
        operation = re.search(r"^ *[\+\-\*\/\^]",expr).group()[-1]
        l_s = re.search(r"\[[\d\,]+\]",expr).group()
        l_s = l_s[1:-1]
        l = l_s.split(',')
        for j in range(len(l)):
            l[j] = str(int(nsp.eval(scalar + operation + l[j])))
        l_s = '['
        for j in l:
            l_s = l_s + j + ","
        l_s = l_s[:-1] + ']'
        s = s.replace(i.group(), l_s, 1)
    return s

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


@client.event
async def on_message(message):
    if message.content.startswith(C_ROLL):
        verbose = False
        s =  message.content[len(C_ROLL):]
        ver = re.search(r"verbose", s)
        if ver:
            verbose=True
            s = s.replace(ver.group(),"")
        nsp = NumericStringParser()
        try:
            repeats = re.search(r"[(repeat)\[]",s)
            if repeats:
                s = prepare_repeats(s,nsp,False)
                s = prepare_dice_expression(s,False)
                s = prepare_scalars(s,nsp)
                if verbose:
                    await client.send_message(message.channel,message.author.mention +": " +s)
                s = insert_scalars_to_repeats(s,nsp)
                await client.send_message(message.channel,('' if verbose else message.author.mention) +": " + s)
            else:
                s = prepare_dice_expression(s,verbose)
                value = str(int(nsp.eval(s)))
                await client.send_message(message.channel,message.author.mention +": " + s + " = " + value)
        except Exception as inst:
            print(inst)
            await client.send_message(message.channel, "Wrongerino")
    




client.run(token)